const app = require('../app');
const supertest = require('supertest');
const client = supertest(app);
var mongoose = require('mongoose');
var User = require('../models/User.js');
var config = require(global.__rootdir + '/config/config.js');
var tokens = require(global.__rootdir + '/test/token.js');

describe('user router', () => {

    beforeAll(async () => {
        await tokens.create();
    });

	afterAll(async () => {
		User.deleteMany({firstName:"Test"});
	});

	var newUser = {
		"firstName": "Test",
		"lastName": "User",
		"username": "test",
		"contactInfo": {
			"email": "test@happyhome.com",
			"phone": "9999999999"
		},
		"password": "password1"
	}

	var newAdminUser = {
		"firstName": "Test",
		"lastName": "Admin",
		"username": "testadmin",
		"contactInfo": {
			"email": "testadmin@happyhome.com",
			"phone": "1111111111"
		},
		"password": "password1"
	}

	var existingUserId = "5de70658e0584934380123a3";


	describe ('POST /users/ creates new Users', () => {
		it('POST /users/ creates a new standard user', async () => {

			const res = await client.post("/users/")
									.send(newUser)
									.set("Content-Type","application/json");

			expect(res.status).toBe(201);
			expect(res.body.id).toBeDefined();

			const createdUser = await User.findOneById(res.body.id);

			expect(createdUser.firstName).toEqual(newUser.firstName);
			expect(createdUser.lastName).toEqual(newUser.lastName);
			expect(createdUser.username).toEqual(newUser.username);
			expect(createdUser.contactInfo.email).toEqual(newUser.contactInfo.email);
			expect(createdUser.contactInfo.phone).toEqual(newUser.contactInfo.phone);
			expect(createdUser.permissionLevel).toEqual(1);
			expect(createdUser.renterScore).toEqual(500);


		});

		it ('POST /users/admin creates a new admin user', async () => {
			

			const res = await client.post("/users/admin")
									.send(newAdminUser)
									.set("Content-Type","application/json")
									.set("authorization", "Bearer " + tokens.adminToken);

			expect(res.status).toBe(201);
			expect(res.body.id).toBeDefined();

			const createdUser = await User.findOneById(res.body.id);

			expect(createdUser.firstName).toEqual(newAdminUser.firstName);
			expect(createdUser.lastName).toEqual(newAdminUser.lastName);
			expect(createdUser.username).toEqual(newAdminUser.username);
			expect(createdUser.contactInfo.email).toEqual(newAdminUser.contactInfo.email);
			expect(createdUser.contactInfo.phone).toEqual(newAdminUser.contactInfo.phone);
			expect(createdUser.permissionLevel).toEqual(2);
		});
	});
	

	describe ('GET /users/ returns Users', () => {
		it('GET / returns all Users', async () => {
			const res = await client.get("/users/")
									.set("authorization", "Bearer " + tokens.adminToken);


			expect(res.status).toBe(200);
			expect(res.body[0]._id).toBeDefined();
		});

		it('GET /?limit=X returns X number of Users', async () => {
			const res = await client.get("/users/")
									.set("authorization", "Bearer " + tokens.adminToken)
									.query({'limit':5});


			expect(res.status).toBe(200);
			expect(res.body.length).toBe(5);
			expect(res.body[0]._id).toBeDefined();
		});

		it('GET /[id] returns User by id', async () => {
			const res = await client.get("/users/" + existingUserId)
									.set("authorization", "Bearer " + tokens.adminToken);

			expect(res.body.firstName).toEqual("Authenticated");
			expect(res.body.lastName).toEqual("User");
			expect(res.body.contactInfo.email).toEqual("authenticated@happyhome.com");
			expect(res.body.contactInfo.phone).toEqual("7777777777");
			expect(res.body.permissionLevel).toEqual(1);
		});

	})
	
});