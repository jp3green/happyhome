const app = require('../app');
const supertest = require('supertest');
var mongoose = require('mongoose');
var config = require(global.__rootdir + '/config/config.js');
var tokens = require(global.__rootdir + '/test/token.js');
var messenger = require(global.__rootdir + '/utils/messaging.js');

describe('messaging utility', () => {
    it('sends an SMS', async () => {
        await messenger.sendSMS('+16134103401','test message')
            .then((msg) => {
                expect(msg).toBeDefined();
                expect(msg.to).toBe('+16134103401');
                expect(msg.body).toBe('Sent from your Twilio trial account - test message');
                expect(msg.status).toBe('queued');
            })
    })
})