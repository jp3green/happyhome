var config = require(global.__rootdir + '/config/config.js');
var mongoose = require('mongoose');

module.exports.setup = async () => {
        await mongoose.connect('mongodb+srv://' + config.get("MongoDB.username") + ':' + config.get("MongoDB.password") + '@' + config.get("MongoDB.clusterUrl") + '/' + config.get("MongoDB.dbName") + '?retryWrites=true&w=majority'
        ).catch(err => { throw err; });
}