const app = require('../app');
const supertest = require('supertest');
const client = supertest(app);
var mongoose = require('mongoose');
require('mongoose-function')(mongoose);
var Task = require('../models/Task.js');
var config = require(global.__rootdir + '/config/config.js');
var tokens = require(global.__rootdir + '/test/token.js');


describe('tasks router', () => {
	
	beforeAll(async () => {
        await tokens.create();
	});

	var simpleTask = {
		"title": "Log a Rent Payment",
		"description": "Keeping track of rent payments is a good way of preventing landlords from charging extra or withholding funds",
		"requirements": [{
			"resourceType": "number",
			"title": "Amount Paid",
			"description": "The amount you paid in rent for this month",
			"required": true
		}],
		onCompletion: (task) => {
			console.log("TASK COMPLETED")
		}
	}

	var existingUserId = "5de70658e0584934380123a3";


	describe ('POST /tasks/ creates new Task', () => {
		it('POST /tasks/ creates a simple task', async () => {
			const res = await client.post("/tasks/")
									.send(simpleTask)
									.set("Content-Type","application/json")
									.set("authorization", "Bearer " + tokens.adminToken);

			expect(res.status).toBe(201);
			expect(res.body._id).toBeDefined();
			expect(res.body.requirements).toBeDefined();
			expect(res.body.requirements[0]._id).toBeDefined();


			const createdTask = await Task.findOneById(res.body._id);

			expect(createdTask.createdDate).toBeDefined();
			expect(createdTask.isCompleted).toEqual(false);
			expect(createdTask.title).toEqual(simpleTask.title);
			expect(createdTask.description).toEqual(simpleTask.description);
			expect(createdTask.requirements[0].resourceType).toEqual(simpleTask.requirements[0].resourceType);
			expect(createdTask.requirements[0].title).toEqual(simpleTask.requirements[0].title);
			expect(createdTask.requirements[0].description).toEqual(simpleTask.requirements[0].description);
			expect(createdTask.requirements[0].required).toEqual(simpleTask.requirements[0].required);
			expect(createdTask.requirements[0].provided).toEqual(false);


		});
	});
	/*
	describe('PUT /tasks/ updated a Task object', () => {
		it ('completes a task if all requirements are met', async () => {
			const res = await client.put("/tasks/")
									.set("authorization", "Bearer " + tokens.adminToken);
		})
	})
	*/
	
	/*
	describe ('GET /users/ returns Users', () => {
		it('GET / returns all Users', async () => {
			const res = await client.get("/users/")
									.set("authorization", "Bearer " + tokens.adminToken);


			expect(res.status).toBe(200);
			expect(res.body[0]._id).toBeDefined();
		});

		it('GET /?limit=X returns X number of Users', async () => {
			const res = await client.get("/users/")
									.set("authorization", "Bearer " + tokens.adminToken)
									.query({'limit':5});


			expect(res.status).toBe(200);
			expect(res.body.length).toBe(5);
			expect(res.body[0]._id).toBeDefined();
		});

		it('GET /[id] returns User by id', async () => {
			const res = await client.get("/users/" + existingUserId)
									.set("authorization", "Bearer " + tokens.adminToken);

			expect(res.body.firstName).toEqual("Authenticated");
			expect(res.body.lastName).toEqual("User");
			expect(res.body.contactInfo.email).toEqual("authenticated@happyhome.com");
			expect(res.body.contactInfo.phone).toEqual("7777777777");
			expect(res.body.permissionLevel).toEqual(1);
		});

	})
	*/
	
});