const app = require('../app');
const supertest = require('supertest');
const client = supertest(app);


//we want to generate different types of tokens 

//Get a master token
const masterRes = await client.post("/auth/")
							.send(
								{
									"email": "master@happyhome.com",
									"password": "password1"
								}
							)
							.then((res) => {
								module.exports.masterToken = res.accessToken
							});

//Get a master token
const authRes = await client.post("/auth/")
							.send(
								{
									"email": "authenticated@happyhome.com",
									"password": "password1"
								}
							)
							.then((res) => {
								module.exports.authenticatedToken = res.accessToken
							});

