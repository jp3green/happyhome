const app = require('../app');
const supertest = require('supertest');
const client = supertest(app);
var mongoose = require('mongoose');
var config = require(global.__rootdir + '/config/config.js');
var db = require(global.__rootdir + '/test/db.js');



describe('app is up and running', () => {

    beforeAll(async () => {
        await db.setup();
    });

	it('/health endpoint returns "OK" message', async () => {
	const res = await client.get("/health");

	expect(res.status).toBe(200);
	expect(res.text).toBe("OK");

	});

	it('/db endpoint returns "Connection OK" message', async () => {
		const res = await client.get("/health/db");

		expect(res.status).toBe(200);
		expect(res.text).toBe("Connection OK");

	});

});
