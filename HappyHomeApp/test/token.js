const app = require('../app');
const supertest = require('supertest');
const client = supertest(app);
var mongoose = require('mongoose');
var config = require('../config/config.js');
var db = require(global.__rootdir + '/test/db.js');

module.exports.create = async () => {
    if (module.exports.token != undefined) return

    await db.setup();
    module.exports.tokens = {}
    //we want to generate different types of tokens 
    
    //Get a master token
    const masterRes = await client.post("/auth/")
                            .send(
                                {
                                    "email": "master@happyhome.com",
                                    "password": "password1"
                                }
                            )
                            .then((res) => {
                                module.exports.masterToken = res.accessToken
                            });


    //Get a master token
    await client.post("/auth/")
        .send(
            {
                "email": "admin@happyhome.com",
                "password": "password1"
            }
        )
        .then((res) => {
            module.exports.adminToken = res.body.accessToken
        });
							
    //Get a master token
    const authRes = await client.post("/auth/")
							.send(
								{
									"email": "authenticated@happyhome.com",
									"password": "password1"
								}
							)
							.then((res) => {
								module.exports.authenticatedToken = res.accessToken
	                        });
}





