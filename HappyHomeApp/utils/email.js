var nodemailer = require('nodemailer');
var config = require(global.__rootdir + '/config/config.js');


let transporter = nodemailer.createTransport({
	    host: config.get("Mail.host"),
	    port: config.get("Mail.secure.port"),
	    secure: config.get("Mail.secure.isSecure"), // true for 465, false for other ports
	    auth: {
	    	user: config.get("Mail.credentials.username"),
	    	pass: config.get("Mail.credentials.password")
    	}
});

//TODO: create email templating utilities
module.exports.sendEmailVerification = (id,email,verification) => {
	module.exports.sendEmail({
		to: email,
		subject: "Verify Your Email",
		text: "To verify your email, please click the link below:\n http://localhost:3000/users/" + id + "/email/verify?code=" + verification,
		html: "To verify your email, please click <a href='http://localhost:3000/users/" + id + "/email/verify?code=" + verification + "'>this link</a>"
	});
}

module.exports.sendEmail = (data) => {
	// send mail with defined transport object
	let info = transporter.sendMail({
	    from: config.get("Mail.credentials.displayName") + " <" + config.get("Mail.credentials.address") + ">", // sender address
	    to: data.to, // list of receivers
	    subject: data.subject, // Subject line
	    text: data.text, // plain text body
	    html: data.html // html body
	},(err) => {
		console.log("Email unable to send: " + err);
	});
}

/*
module.exports.sendBulkEmail = (data, recipients) => {
	// send mail with defined transport object
	let info = transporter.sendMail({
	    from: config.get("Mail.credentials.address"), // sender address
	    to: 'jp3green@uwaterloo.ca', // list of receivers
	    subject: 'Test Email', // Subject line
	    text: 'Test Email Text', // plain text body
	    html: '<b>Test</b> Email <i>Text</i>' // html body
	},(err) => {
		console.log("Email unable to send: " + err);
	});
}
*/