var config = require(global.__rootdir + '/config/config.js');
const accountSid = config.get("Twilio.sid");
const authToken = config.get("Twilio.token");
const client = require('twilio')(accountSid, authToken);

module.exports.sendSMS = (to,msg) => {
    return new Promise((resolve,reject) => {
        client.messages
            .create({
                body: msg,
                from: config.get("Twilio.number"),
                to: to
            })
            .then(message => {
                resolve(message);
            })
            .catch(err => reject(err));
    })
}
