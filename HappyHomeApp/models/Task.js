'use strict';
const mongoose = require('mongoose');
require('mongoose-function')(mongoose);
const Schema = require('mongoose').Schema;
const ObjectId = Schema.Types.ObjectId;
const Mixed = Schema.Types.Mixed;
const taskSchema = Schema({
							createdDate: {type: Date, default: Date.now()},
							isCompleted: {type: Boolean, default: false},
							completedDate: Date,
							title: String,
							description: String,
							requirements: [{
								resourceType: {type: String, enum: ['photo','url','email','document','number','string','date','id']},
								title: String,
								description: String,
								required: {type: Boolean, default: true},
								value: Mixed,
								provided: {type: Boolean, default: false},
								providedDate: Date
							}],
							onCompletion: Function
						});

/* global db */
var Task = mongoose.model('Task', taskSchema, 'task');

module.exports.create = (body) => {
	return new Promise((resolve,reject) => {

        if (body.onCompletion && typeof body.onCompletion == "object") {
        	body.onCompletion = Function.apply(undefined,body.onCompletion);
        }
        else if (typeof body.onCompletion == 'function') {
        	body.onCompletion = body.onCompletion.toString();
        }

		var newTask = new Task(body);
	    newTask.save(function(err,task) {
	    	if (err) reject(err);
	    	resolve(task);
	    });
	});
};

module.exports.list = (limit) => {
	return new Promise((resolve,reject) => {
		module.exports.find({},limit)
		.then((tasks) => {
		  	resolve(tasks);
	  	})
	    .catch((err) => {
	        reject(err);
	    });
	});	
}

module.exports.listDefault = (limit) => {
	return new Promise(async (resolve,reject) => {
		module.exports.find({default: true},limit)
		.then((tasks) => {
		  	resolve(tasks);
	  	})
	    .catch((err) => {
	        reject(err);
	    });
	});	
}

module.exports.find = async (params,limit = 25) => {
	return new Promise(async (resolve,reject) => {
		await Task.find(params).limit(limit).exec((err,tasks) => {
			if (err) reject(err);
			if (tasks) {
				tasks.forEach((j) => {
					j.__v = undefined;
				    j.id = j._id;
					j._id = undefined;
				});
			}
		  	resolve(tasks);
  		});
	});	
}

module.exports.findOneById = (id) => {
	return new Promise((resolve,reject) => {
		Task.findOne({_id: id})
		.then((task) => {
		  	resolve(task);
	  	})
	    .catch((err) => {
	        reject(err);
	    });
	});	
	
}

var isNewlyCompleted = (task) => {
	if (task.isCompleted) return false; //return false since a newly completed task will not have the isCompleted field set to true
	task.requirements.forEach((req) => {
		if (!req.provided && req.required) {
			return false;
		}
	});
	return true;
}

module.exports.updateOne = (id,newTask) => {
	return new Promise((resolve,reject) => {

		Promise.all(newTask.requirements.map((req) => {
			return new Promise((rs,rj) => {
				module.exports.updateTaskRequirement(id,req._id,req)
				.then(() => rs())
				.catch((err) => rj(err));
			})
			
		}))
		.then(() => {
			delete newTask.requirements; //delete the requirements so it doesn't get updated twice
			Task.updateOne({_id: id}, newTask, function(err) {
	    	if (err) reject(err);
	    	module.exports.findOneById(id).then((updatedTask) => {
	    		if (isNewlyCompleted(updatedTask)) {
		    		updatedTask.isCompleted = true;
		    		updatedTask.completedDate = Date.now()
		    		updatedTask.onCompletion(updatedTask)
		    		.then(() => {
		    			updatedTask.save()
			    		.then(() => {
			    			resolve();
			    		})
			    		.catch((err) => {
			    			reject(err);
			    		})
		    		})
		    		.catch((err) => reject(err));
		    		
		    	}
		    	else {
		    		resolve();
		    	}
	    	})
	    	.catch((err) => reject(err));
		})
		.catch((err) => reject(err));
		
    	});
	});	
} 

module.exports.updateTaskRequirement = (taskId,requirementId,newReq) => {
	return new Promise((resolve,reject) => {
		Task.updateOne({_id: taskId, "requirements._id": requirementId}, 
						{"$set": 
								{
									"requirements.$.value": newReq.value,
									"requirements.$.provided": newReq.provided
								}
						}
		)
		.then(() => resolve())
		.catch((err) => reject(err));
	})
}

module.exports.deleteOne = (id) => {
	return new Promise((resolve,reject) => {
		Task.deleteOne({_id: id}, function (err) {
			if (err) reject(err);
			resolve();
		});
	});	
} 