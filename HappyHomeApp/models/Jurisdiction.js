'use strict';
const mongoose = require('mongoose')
const Schema = require('mongoose').Schema;
const ObjectId = Schema.Types.ObjectId;
const Mixed = Schema.Types.Mixed;
const jurisdictonSchema = Schema({
							rentCeiling: Number,
							rentFloor: Number,
							city: String,
							province: String,
							country: String,
							homesAffected: [ObjectId]
						});

/* global db */
var Jurisdiction = mongoose.model('Jurisdiction', jurisdictonSchema, 'jurisdiction');

module.exports.create = (body) => {
	return new Promise((resolve,reject) => {
		var newJurisdiction = new Jurisdiction(body);
	    newJurisdiction.save(function(err,jurisdiction) {
	    	if (err) reject(err);
	    	resolve(jurisdiction);
	    });
	});
};

module.exports.list = (limit) => {
	return new Promise((resolve,reject) => {
		module.exports.find({},limit)
		.then((jurisdictions) => {
		  	resolve(jurisdictions);
	  	})
	    .catch((err) => {
	        reject(err);
	    });
	});	
}

module.exports.find = async (params,limit = 25) => {
	return new Promise(async (resolve,reject) => {
		await Jurisdiction.find(params).limit(limit).exec((err,jurisdictions) => {
			if (err) reject(err);
			if (jurisdictions) {
				jurisdictions.forEach((j) => {
					j.__v = undefined;
				    j.id = j._id;
					j._id = undefined;
				});
			}
		  	resolve(jurisdictions);
  		});
	});	
}

module.exports.findOneById = (id) => {
	return new Promise((resolve,reject) => {
		module.exports.find({_id: id},1)
		.then((jurisdictions) => {
		  	resolve(jurisdictions);
	  	})
	    .catch((err) => {
	        reject(err);
	    });
	});	
	
}

module.exports.updateOne = (id,newJurisdiction) => {
	return new Promise((resolve,reject) => {
		Jurisdiction.updateOne({_id: id}, newJurisdiction, function(err,jurisdiction) {
	    	if (err) reject(err);
	    	resolve();
    	});
	});	
} 

module.exports.deleteOne = (id) => {
	return new Promise((resolve,reject) => {
		Jurisdiction.deleteOne({_id: id}, function (err) {
			if (err) reject(err);
			resolve();
		});
	});	
} 