const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = require('mongoose').Schema;
const ObjectId = Schema.Types.ObjectId;
const Mixed = Schema.Types.Mixed;
const Home = require(global.__rootdir + '/models/Home');
const Task = require(global.__rootdir + '/models/Task');
var crypto = require('crypto');
var mailer = require(global.__rootdir + '/utils/email');
const userSchema = Schema({	
							firstName: {type: String, required: true}, 
							lastName: {type: String, required: true},
							username: String,
							createdDate: {type: Date, default: Date.now()},
							contactInfo: {
								email: {type: String, unique: true, required: true},
								phone: {type: String, unique: true, required: true}
							},
							leases: [{
								_id: ObjectId,
								isLandlord: {Boolean, default: false}
							}],
							permissionLevel: {type: Number, default: 1},
							passwordHash: {type: String, required: true},
							location: {
								lat: Number, //might need to be mongoose.Decimal128
								lon: Number //might need to be mongoose.Decimal128
							},
							university: {
								_id: ObjectId,
								verified: Boolean
							},
							renterScore: {type: Number, default: 500},
							transactions: [{
								transactionType: {type: String, enum: ['addTask', 'completeTask', 'addHome', 'removeHome', 'subletHome', 'addLease']},
								completionTime: Date,
								value: Mixed
							}],
							settings: {

							},
							tasks: [ObjectId]
						});

/* global db */
var User = mongoose.model('User', userSchema, 'user');


module.exports.create = (body,isAdmin) => {
	return new Promise((resolve,reject) => {

		//delete fields that can only be set by us
		delete body.emailVerified;
		delete body.emailVerificationCode;

		//if user tries to give themselves permissions set them back
		if (isAdmin) {
			body.permissionLevel = 2;
		}
		else {
			body.permissionLevel = 1;
		}

		//create a pssword hash
	    var salt = crypto.randomBytes(16).toString('base64');
	    var hash = crypto.createHmac('sha512',salt)
	                                    .update(body.password)
	                                    .digest("base64");

	    body.passwordHash = salt + "$" + hash;
	    //Make sure the unencrypted password is not stored
	    delete body.password;

	    //create a email verification code
	    body.emailVerificationCode = crypto.randomBytes(16).toString('base64'); 

		var newUser = new User(body);
		newUser.save((err,user) => {
			if (err) {
				reject(err)
			}
			else {
				resolve(user);
				//add the default tasks after the fact
				getDefaultTasks().forEach((t) => module.exports.addTask(user._id,t).catch((err) => reject(err)));
			}
			
			
			

		});
		
	    
	});
};

var getDefaultTasks = () => {
	return [{
		title: "Add a Lease",
		description: "Get started with HappyHome by adding a Lease",
		requirements: [{
			"resourceType": "id",
			"title": "Lease ID",
			"description": "The ID of the lease",
			"required": true
		}],
		onCompletion: function anonymous(task) {
			return new Promise((resolve,reject) => {
				var User = require(global.__rootdir + '/models/User');
				User.findOneByTaskId(task._id)
				.then((user) => {
					user.leases.push(task.requirements.find(x => x.resourceType == "id").value);
					user.save()
					.then(() => {
						resolve();
						module.exports.addTransaction(id,'addLease');
					})
					.catch(err => reject(err));
				})
				.catch((err) => reject(err));
			})
		}
	}]
}

module.exports.list = (limit) => {
	return new Promise((resolve,reject) => {
		User.find().limit(limit).skip(0).exec((err,users) => {
			if (err) reject(err);
		  	resolve(users);
  		});
	});	
}

module.exports.findOneById = (id) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id: id},(err,user) => {
		  	if (err) reject(err);
		  	resolve(user);
  		});
	});	
} 

module.exports.findOneByEmail = (email) => {
	return new Promise((resolve,reject) => {
		User.findOne({"contactInfo.email": email},(err,user) => {
		  	if (err) reject(err);
		  	resolve(user);
  		});
	});	
} 

module.exports.findOneByTaskId = (email) => {
	return new Promise((resolve,reject) => {
		User.findOne({tasks: email},(err,user) => {
		  	if (err) reject(err);
		  	resolve(user);
  		})
  		.catch(err => reject(err));
	});	
} 

module.exports.find = (params) => {
	return new Promise((resolve,reject) => {
		User.find(params,(err,user) => {
		  	if (err) reject(err);
		  	resolve(user);
  		});
	});	
} 

module.exports.updateOne = (id,newUser) => {
	return new Promise((resolve,reject) => {

		//delete fields that can only be set by us
		delete body.emailVerified;
		delete body.emailVerificationCode;

		//if user tries to give themselves permissions set them back
		if (body.permissionLevel > 1 || body.permissionLevel > 1) {
			body.permissionLevel = 1;
		}

		User.updateOne({_id: id}, newUser, function(err,user) {
	    	if (err) reject(err);
	    	resolve();
    	});
	});	
} 


module.exports.addTransaction = async (id,transactionType,val = null) => {
	return new Promise((resolve,reject) => {
		var transaction = {
			transactionType: transactionType,
			completionTime: Date.now(),
			value: val 
		};

		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);
	    	user.transactions.push(transaction);
	    	user.save()
	    	.then(() => {
	    		module.exports.evaluateRenterScore(id);
	    		resolve();
	    	})
	    	.catch((err) => {
	    		if (err) reject(err);
	    	});
    	});
	});
}

module.exports.addTask = (id,task) => {
	return new Promise((resolve,reject) => {
		Task.create(task)
		.then((res) => {
			User.findOne({_id: id})
			.then((user) => {
				user.tasks.push(res.id);

				user.save()
					.then((user) => {
						module.exports.addTransaction(id,'addTask',res.id);
						resolve();
					})
					.catch((err) => {
						reject(err);
					})
			});
		})
		.catch((err) => {
			reject(err);
		});
	});
}

module.exports.getTasks = (id) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id: id})
		.then((user) => {
			Promise.all(user.tasks.map(taskId => Task.findOneById(taskId)))
			.then((tasks) => {
				resolve(tasks);
			})
			.catch((err) => {
				reject(err)
			});
		})
		.catch((err) => {
			reject(err);
		});
	});
}

module.exports.updateTasks = (id,newTasks) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id: id})
		.then((user) => {
			var incompleteTasks = [];
			Promise.all(user.tasks.map(t => {
				return new Promise((rs,rj) => {
					var newTask = newTasks.find(x => x._id == t);
					if (newTask) {
						Task.updateOne(t,newTask)
						.then(() => {
							Task.findOneById(t)
							.then((task) => {
								if (task.isCompleted) {
									module.exports.addTransaction(id,'completeTask',t);
								}
								else {
									incompleteTasks.push(t);
								}
								rs();
							})
						//Take out any completed tasks 
							.catch((err) => {
								rj(err);
							});
						})
						.catch((err) => rj(err));
					}
					else {
						incompleteTasks.push(t);
						rs();
					}	
				})	
			}))
			.then(() => {

				User.findOne({_id: user._id})
				.then((updatedUser) => {
					updatedUser.tasks = incompleteTasks;
					updatedUser.save()
					.then(() => {
						resolve();
					})
					.catch((err) => {
						reject(err);
					});
				})
				
			})	
		})
		.catch((err) => {
			reject(err);
		});
	});
}

module.exports.updateLocation = (id, lat, long) => {
	return new Promise((resolve,reject) => {
		var location  = {
			latitude: lat,
			longitude: long
		};
		
		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);
	    	user.location = location;
	    	user.save((err) => {
	    		if (err) reject(err);
	    	});
	    	resolve();
    	});
	});
}

module.exports.evaluateRenterScore = async (id) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id: id}, function(err,user) {
	    	if (err) reject(err);

	    	//TODO: come up with a way more sophisticated renter score calculation
	    	user.renterScore += 1;

	    	user.save((err) => {
	    		if (err) reject(err);
	    	});
	    	resolve();
    	});
	});
}
/*
module.exports.verifyEmail = (id,code) => {
	return new Promise((resolve,reject) => {
		User.findOne({_id:id}, function (err,user) {
			if (err) reject(err);
			if (code == user.emailVerificationCode) {
				user.emailVerified = true;
				user.save(function (err,user) {
					if (err) reject(err);
					resolve(user);
				});
			}
			else {
				reject(new Error("Email Verification Code incorrect"));
			}
		});
	});	
}
*/
module.exports.deleteOne = (id) => {
	return new Promise((resolve,reject) => {
		User.deleteOne({"_id": id}, function (err,user) {
			if (err) reject(err);
			resolve();
		});
	});	
} 

module.exports.deleteMany = (params) => {
	return new Promise((resolve,reject) => {
		User.deleteMany(params, function (err,user) {
			if (err) reject(err);
			resolve();
		});
	});	
} 


