'use strict';
const mongoose = require('mongoose')
const Schema = require('mongoose').Schema;
const ObjectId = Schema.Types.ObjectId;
const Jurisdiction = require(global.__rootdir + '/models/Jurisdiction');
const Task = require(global.__rootdir + '/models/Task');
const Mixed = Schema.Types.Mixed;
const homeSchema = Schema({
							address: {
								houseNumber: {type: Number, min: 1},
								streetName: String, 
								city: String,
								unitNumber: Number,
								roomNumber: Number,
								province: String, 
								postalCode: String
							},
							jurisdictionIds: [ObjectId],
							ownerId: ObjectId,
							photoUrls: [String],
							tasks: [ObjectId]
						});

/* global db */
var Home = mongoose.model('Home', homeSchema, 'home');

module.exports.create = (body) => {
	return new Promise(async (resolve,reject) => {

		//find all necessary Jurisidictions and add them 
		body.jurisdictionIds = [];
		await Jurisdiction.find({
			$or: [
				{city: body.address.city},
				{ 
					$and: [
						{city: undefined},
						{province: body.address.province}
					]
				},
				{ 
					$and: [
						{city: undefined},
						{province: undefined},
						{province: body.address.province}
					]
				},
			]
		})
		.then((jurisdictions) => {
			if (jurisdictions) {
				jurisdictions.forEach((j) => {
					body.jurisdictionIds.push(j._id);
				});
			}
		})
		.catch((err) => {
			reject(err);
		});
		
		//Add all default tasks
		body.tasks = [];
		await Task.listDefault()
		.then((tasks) => {
			tasks.forEach((t) => {
				//turn the id into a taskId
				t.taskId = t._id;
				delete t._id;
				body.tasks.push(t);
			});
		})
		.catch((err) => {
			reject(err)
		});

		var newHome = new Home(body);
	    newHome.save(function(err,home) {
	    	if (err) reject(err);
	    	resolve(home);
	    });
	});
};

module.exports.find = (limit) => {
	return new Promise((resolve,reject) => {
		Home.find().limit(limit).exec((err,homes) => {
			homes.forEach((home) => {
				home.__v = undefined;
			    home.id = home._id;
				home._id = undefined;
			});
		  	if (err) reject(err);
		  	resolve(homes);
  		});
	});	
}

module.exports.findOneById = (id) => {
	return new Promise((resolve,reject) => {
		Home.findOne({_id: id},(err,home) => {
	       	home.__v = undefined;
	        home.id = home._id;
			home._id = undefined;
		  	if (err) reject(err);
		  	resolve(home);
  		});
	});	
}

module.exports.updateOne = (id,newHome) => {
	return new Promise((resolve,reject) => {
		Home.updateOne({_id: id}, newHome, function(err,home) {
	    	if (err) reject(err);
	    	resolve();
    	});
	});	
} 

module.exports.deleteOne = (id) => {
	return new Promise((resolve,reject) => {
		Home.deleteOne({_id: id}, function (err) {
			if (err) reject(err);
			resolve();
		});
	});	
}

module.exports.sublet = (id,userId,subletteeId) => {
	return new Promise((resolve,reject) => {
		Home.findOne({_id: id}, (err,home) => { //make sure to find the unsubletted Home
			if (err) reject(err);

			//add new renter in home
			home.renters.push({
				_id: subletteeId,
				current: true,
				occupying: true,
				startDate: new Date()
			});

			//set subletting renter to not occupying
			home.renters.map((renter) => {
				if (renter._id == userId) {
					renter.occupying = false;
				}
			});

			var sublet = new Home({
				address: home.address,
				lease: home.lease,
				jurisdictionIds: home.jurisdictionIds,
				renters: home.renters,
				subletter: {
					_id: userId,
					homeId: home._id
				},
				landlordId: home.landlordId,
				tasks: home.tasks
			});

			sublet.save((err,sublet) => {
				if (err) reject(err);
				home.sublettee = {
					_id: subletteeId,
					homeId: sublet._id
				};

				home.save((err) => {
					if (err) reject(err);
					resolve();
				});
			});
			
				
		});
	});	
}