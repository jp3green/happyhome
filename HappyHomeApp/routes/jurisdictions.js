var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Jurisdiction = require('../models/Jurisdiction.js');
var auth = require(global.__rootdir + '/auth/auth');

//CREATE Jurisdictions listing
router.post('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Jurisdiction.create(req.body).then((jurisdiction) => {
            res.status(201).json({"id": jurisdiction._id}).end();
        });
    }
]);

// GET Jurisdictions listing.
router.get('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        var limit = req.query.limit ? parseInt(req.query.limit) : 25 //return 25 if no limit is set
    	Jurisdiction.list(limit)
        .then((jurisdictions) => {
    	  	res.status(200).json(jurisdictions).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while creating the resource" + err]}).end();
        });
    }
]);

// READ single Jurisdiction
router.get('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Jurisdiction.findOneById(req.params._id)
        .then((jurisdiction) => {;
    	  	res.status(200).json(jurisdiction).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// UPDATE Jurisdictions listing
router.put('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Jurisdiction.updateOne(req.params._id,req.body)
        .then(() => {
        	res.status(204).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource"]}).end();
        });
    }
]);

// DELETE Jurisdictions listing
router.delete('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
    	Jurisdiction.deleteOne(req.params._id)
        .then(() => {
    		res.status(204).end();
    	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while deleting the resource"]}).end();
        });
    }
]);

module.exports = router;