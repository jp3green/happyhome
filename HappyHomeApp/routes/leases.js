var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
const Lease = require(global.__rootdir + '/models/Lease');
var auth = require(global.__rootdir + '/auth/auth');


//CREATE Leases listing
router.post('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Lease.create(req.body)
        .then((lease) => {
            res.status(201).json({"id": lease._id}).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while creating the resource"]}).end();
        });
    }
]);

// GET Leases listing.
router.get('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        var limit = req.query.limit ? parseInt(req.query.limit) : 25 //return 25 if no limit is set
    	Lease.find(limit)
        .then((leases) => {
    	  	res.status(200).json(leases).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// READ single Lease
router.get('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        Lease.findOneById(req.params._id)
        .then((lease) => {;
    	  	res.status(200).json(lease).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// UPDATE Leases listing
router.put('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
        Lease.updateOne(req.params._id,req.body)
        .then(() => {
        	res.status(204).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource"]}).end();
        });
    }
]);

// DELETE Leases listing
router.delete('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.USER),
    (req, res, next) => {
    	Lease.deleteOne(req.params._id)
        .then(() => {
    		res.status(204).end();
    	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while deleting the resource"]}).end();
        });
    }
]);

module.exports = router;