var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Task = require('../models/Task.js');
var auth = require(global.__rootdir + '/auth/auth');

//CREATE Tasks listing
router.post('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Task.create(req.body).then((task) => {
            res.status(201).json({"_id": task._id, "requirements": task.requirements.map((r) => { return {"_id": r._id}})}).end();
        });
    }
]);

// GET Tasks listing.
router.get('/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        var limit = req.query.limit ? parseInt(req.query.limit) : 25 //return 25 if no limit is set
    	Task.list(limit)
        .then((tasks) => {
    	  	res.status(200).json(tasks).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while creating the resource" + err]}).end();
        });
    }
]);

// READ single Task
router.get('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Task.findOneById(req.params._id)
        .then((task) => {;
    	  	res.status(200).json(task).end();
      	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while getting the resource"]}).end();
        });
    }
]);

// UPDATE Tasks listing
router.put('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
        Task.updateOne(req.params._id,req.body)
        .then(() => {
        	res.status(204).end();
        })
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while updating the resource:" + err]}).end();
        });
    }
]);

// DELETE Tasks listing
router.delete('/:_id/', [
    auth.verifyJWTToken,
    auth.verifyAuthorization(auth.level.ADMIN),
    (req, res, next) => {
    	Task.deleteOne(req.params._id)
        .then(() => {
    		res.status(204).end();
    	})
        .catch((err) => {
            res.status(500).json({"errors": ["Something went wrong while deleting the resource"]}).end();
        });
    }
]);

module.exports = router;