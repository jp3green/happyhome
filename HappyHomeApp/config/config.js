var config = require('config');

module.exports.get = (index) => {
	if (config.has(index)) {
		return config.get(index);
	}
	else {
		//must replace with underscore since Gitlab doesn't allow env variables with dots
		return process.env[index.replace('.','_')];
	}
}